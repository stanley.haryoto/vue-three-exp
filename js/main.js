/*

This file is part of vue-three-exp project.

vue-three-exp is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

vue-three-exp is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with vue-three-exp.  If not, see <http://www.gnu.org/licenses/>.

*/

new Vue({
  el: "#app",

  data() {
    return {
      scene: null,
      camera: null,
      renderer: null,
      geometries: []
    }
  },

  beforeCreate() {
    console.log( 'beforeCreate' );

  },

  created() {

    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setSize( window.innerWidth, window.innerHeight );

    console.log( 'created' );

  },

  beforeMount() {
    console.log( 'beforeMount' );

  },

  mounted() {
    console.log( 'mounted' );

    let domElement = this.$el;
    domElement.appendChild( this.renderer.domElement );

    let geometry = new THREE.BoxGeometry( 1, 1, 1 );
		let material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
		let cube = new THREE.Mesh( geometry, material );
    this.geometries.push( cube );
		this.scene.add( cube );

    this.camera.position.z = 5;

    this.animate();
  },

  methods: {
    animate: function() {
      requestAnimationFrame( this.animate );

      let cube = this.geometries[0];
      cube.rotation.x += 0.1;
      cube.rotation.y += 0.1;

      this.renderer.render( this.scene, this.camera );
    }
  }
});
